**URL Shortening Service**

A simple URL shortening service.A stand-alone application that provides URL shortening, redirection etc. as RESTful API.


**Built With**

 -  Spring Boot
-  Spring Validation
-  Java 11
-  Spring Data JPA
-  Postgres
-  GitLab – as source code management
-  RabbitMq
-  Liquibase 

   
   - RabbitMq is used for speed up response time because app is not waiting to log request and response and return the response as soon as ready!

* **Visitor**
* **Strategy**
* **Template** patterns have been used to provide easy **extensibility, clean code.**

** **Unit** **Tests** are written.

** **OOP** **principles** and **SOLID** have been taken into consideration.

** In order to parse and build a Url, **UriComponentsBuilder** which is Spring solution has been preferred. 

-docker-compose up will create you db.

-**Liquibase** has been used as a DB maintain tool so when the app being run; db tables will be created.

**A collection json has been created to test on Postman.** Once you run "long to short" request, you can use the "short link" (which is the "**data**" in output) to test "short to long" request.
``


**_Following steps could be used to run the app:_**

**Run**

mvn clean install

docker-compose up --build **or** startup.sh

**Test**

mvn test

**MBMDSPNXT.postman_collection.json contains requests to test.**

**Apis**

Two (2) end-points that are;

1- shortlink-to-longlink method: post
url: http://localhost:8080/api/convert/shortlink-to-longlink
request body: { "url": " http://merced.es/GUKA8w "}

2- longlink-to-shortlink
method: post
url: http://localhost:8080/api/convert/longlink-to-shortlink
request body: { "url": " https://group.mercedes-benz.com/karriere/berufserfahrene/direkteinstieg "}


**Deployment**

There is no need of deployment.

**Built By**

Özlem Yıldırım

**Acknowledgements**

The project is based on the instructions that was forwarded by MBM.
