package com.mbm.link.converter.restcontroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbm.link.converter.dto.UriDto;
import com.mbm.link.converter.linkconverter.api.LinkConverterRestController;
import com.mbm.link.converter.linkconverter.dto.LinkConverterDto;
import com.mbm.link.converter.linkconverter.dto.ShortLink;
import com.mbm.link.converter.linkconverter.dto.LongLink;
import com.mbm.link.converter.linkconverter.mapper.ConversionMapper;
import com.mbm.link.converter.linkconverter.model.ConversionType;
import com.mbm.link.converter.linkconverter.model.LinkConverter;
import com.mbm.link.converter.linkconverter.repository.LinkConverterRepository;
import com.mbm.link.converter.linkconverter.service.ConversionHistoryService;
import com.mbm.link.converter.linkconverter.service.ConversionHistoryServiceImp;
import com.mbm.link.converter.linkconverter.service.LongLinkToShortLinkConverterService;
import com.mbm.link.converter.linkconverter.service.ShortLinkToLongLinkConverterService;
import com.mbm.link.converter.utils.LocaleAwareMessageProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(LinkConverterRestController.class)
public class LinkConverterRestControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LongLinkToShortLinkConverterService longLinkToShortLinkConverterService;

    @MockBean
    private ShortLinkToLongLinkConverterService shortLinkToLongLinkConverterService;

    @MockBean
    private LocaleAwareMessageProvider messageProvider;


    @Test
    public void checkGiven_Request_Incorrect_for_Long_to_short() throws Exception {

        UriDto uriDto = UriDto.builder().url("httpsa://group.google.com/3karriere/berufserfahrene/direkteinstieg").build();
        ShortLink shortLink = new ShortLink("http://merced.es/GUKA8w");

        given((ShortLink) longLinkToShortLinkConverterService.prepareLink(any())).willReturn(shortLink);

        mvc.perform(post("/api/convert/longlink-to-shortlink")
                        .content(asJsonString(uriDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void checkGiven_Request_Correct_for_Long_to_short() throws Exception {

        UriDto uriDto = UriDto.builder().url("https://group.mercedes-benz.com/karriere/berufserfahrene/direkteinstieg").build();
        ShortLink shortLink = new ShortLink("http://merced.es/GUKA8w");

        given((ShortLink) longLinkToShortLinkConverterService.prepareLink(any())).willReturn(shortLink);

        mvc.perform(post("/api/convert/longlink-to-shortlink")
                        .content(asJsonString(uriDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }


    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
