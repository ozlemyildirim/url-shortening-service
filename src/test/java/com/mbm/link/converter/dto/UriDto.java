package com.mbm.link.converter.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UriDto {
    private String url;
}
