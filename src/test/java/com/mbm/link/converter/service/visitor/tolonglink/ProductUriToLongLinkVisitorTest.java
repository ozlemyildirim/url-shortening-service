package com.mbm.link.converter.service.visitor.tolonglink;

import com.mbm.link.converter.linkconverter.dto.ShortLink;
import com.mbm.link.converter.linkconverter.dto.Uri;
import com.mbm.link.converter.linkconverter.dto.UriVisitor;
import com.mbm.link.converter.linkconverter.dto.LongLink;
import com.mbm.link.converter.linkconverter.service.visitor.tolonglink.ProductUriToLongLinkVisitor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ProductUriToLongLinkVisitorTest {

    @TestConfiguration
    static class ProductUriToLongLinkVisitorTestContextConfiguration {

        @Bean
        public UriVisitor uriVisitor() {
            return new ProductUriToLongLinkVisitor();
        }
    }

    @Autowired
    private UriVisitor uriVisitor;

      @Test
      public void default_Check_Method() throws Exception {
        ShortLink shortLink = new ShortLink("http://merced.es/GUKA8w");
        boolean result = uriVisitor.check(shortLink);
        assertThat(result).isEqualTo(true);
    }


}
