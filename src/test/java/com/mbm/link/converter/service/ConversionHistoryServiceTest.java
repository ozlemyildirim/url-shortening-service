package com.mbm.link.converter.service;

import com.mbm.link.converter.linkconverter.dto.LinkConverterDto;
import com.mbm.link.converter.linkconverter.mapper.ConversionMapper;
import com.mbm.link.converter.linkconverter.model.ConversionType;
import com.mbm.link.converter.linkconverter.model.LinkConverter;
import com.mbm.link.converter.linkconverter.repository.LinkConverterRepository;
import com.mbm.link.converter.linkconverter.service.ConversionHistoryService;
import com.mbm.link.converter.linkconverter.service.ConversionHistoryServiceImp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConversionHistoryServiceTest {

    @Autowired
    ConversionHistoryService conversionHistoryService;

    @MockBean
    ConversionMapper conversionMapper;

    @MockBean
    LinkConverterRepository linkConverterRepository;

    @Test
    public void test_Save_ConverterLink() throws Exception {

        LinkConverterDto linkConverterDto = LinkConverterDto.builder()
                .conversionType(ConversionType.LONG_TO_SHORTLINK)
                .convertedUrl("http://merced.es/jobs")
                .sourceUrl("https://group.mercedes-benz.com/karriere/jobsuche")
                .build();

        LinkConverter linkConverter = LinkConverter.builder()
                .conversionType(ConversionType.LONG_TO_SHORTLINK)
                .convertedUrl("http://merced.es/jobs")
                .sourceUrl("https://group.mercedes-benz.com/karriere/jobsuche")
                .build();

        given(conversionMapper.createLinkConverter(linkConverterDto)).willReturn(linkConverter);
        conversionHistoryService.saveConversion(linkConverterDto);

        ArgumentCaptor<LinkConverter> captor = ArgumentCaptor.forClass(LinkConverter.class);
        verify(linkConverterRepository).save(captor.capture());
        assertEquals(captor.getValue().getConvertedUrl(), linkConverterDto.getConvertedUrl());
    }

}
