package com.mbm.link.converter.service;

import com.mbm.link.converter.linkconverter.dto.ShortLink;
import com.mbm.link.converter.linkconverter.dto.Uri;
import com.mbm.link.converter.linkconverter.dto.LongLink;
import com.mbm.link.converter.linkconverter.producer.LinkConverterProducer;
import com.mbm.link.converter.linkconverter.service.LongLinkToShortLinkConverterService;
import com.mbm.link.converter.linkconverter.service.ShortLinkToLongLinkConverterService;
import com.mbm.link.converter.utils.LocaleAwareMessageProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class LongLinkToShortConverterServiceTest {

    @Autowired
    private LongLinkToShortLinkConverterService longLinkToShortLinkConverterService;

    @MockBean
    private LocaleAwareMessageProvider messageProvider;

    @MockBean
    private LinkConverterProducer linkConverterProducer;

    @Test
    public void productLongLink_to_ShortLink() throws Exception {
        ShortLink shortLink = new ShortLink("http://merced.es/GUKA8w");
        LongLink longLink = new LongLink("https://group.mercedes-benz.com/karriere/berufserfahrene/direkteinstieg");
        Uri uri = longLinkToShortLinkConverterService.prepareLink(longLink);

        assertThat(shortLink.getHost()).isEqualTo(uri.getHost());
        assertThat(shortLink.getScheme()).isEqualTo(uri.getScheme());
        assertThat(shortLink.getParams()).isEqualTo(uri.getParams());
        assertThat(shortLink.getPathSegments().size()).isEqualTo(uri.getPathSegments().size());
    }


}
