package com.mbm.link.converter.service;

import com.mbm.link.converter.linkconverter.dto.LinkConverterDto;
import com.mbm.link.converter.linkconverter.dto.LongLink;
import com.mbm.link.converter.linkconverter.dto.ShortLink;
import com.mbm.link.converter.linkconverter.dto.Uri;
import com.mbm.link.converter.linkconverter.mapper.ConversionMapper;
import com.mbm.link.converter.linkconverter.model.ConversionType;
import com.mbm.link.converter.linkconverter.model.LinkConverter;
import com.mbm.link.converter.linkconverter.producer.LinkConverterProducer;
import com.mbm.link.converter.linkconverter.repository.LinkConverterRepository;
import com.mbm.link.converter.linkconverter.service.ConversionHistoryService;
import com.mbm.link.converter.linkconverter.service.ShortLinkToLongLinkConverterService;
import com.mbm.link.converter.utils.LocaleAwareMessageProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ShortLinkToLongConverterServiceTest {

    @MockBean
    private ShortLinkToLongLinkConverterService shortLinkToLongConverterService;

    @Autowired
    ConversionHistoryService conversionHistoryService;
    @MockBean
    ConversionMapper conversionMapper;

    @MockBean
    LinkConverterRepository linkConverterRepository;

    @MockBean
    private LocaleAwareMessageProvider messageProvider;

    @MockBean
    private LinkConverterProducer linkConverterProducer;

    @MockBean
    private Uri uri;

    @Before
    public void setup() {
        when(shortLinkToLongConverterService.getLink(Mockito.any())).thenReturn(new LongLink("https://group.mercedes-benz.com/karriere/berufserfahrene/direkteinstieg"));
    }

    @Test
    public void productShortLink_to_LongLink() throws Exception {


        LinkConverterDto linkConverterDto = LinkConverterDto.builder()
                .conversionType(ConversionType.LONG_TO_SHORTLINK)
                .convertedUrl("http://merced.es/GUKA8w")
                .sourceUrl("https://group.mercedes-benz.com/karriere/berufserfahrene/direkteinstieg")
                .build();

        LinkConverter linkConverter = LinkConverter.builder()
                .conversionType(ConversionType.LONG_TO_SHORTLINK)
                .convertedUrl("http://merced.es/GUKA8w")
                .sourceUrl("https://group.mercedes-benz.com/karriere/berufserfahrene/direkteinstieg")
                .build();

        given(conversionMapper.createLinkConverter(linkConverterDto)).willReturn(linkConverter);
        conversionHistoryService.saveConversion(linkConverterDto);

        ArgumentCaptor<LinkConverter> captor = ArgumentCaptor.forClass(LinkConverter.class);
        verify(linkConverterRepository).save(captor.capture());
        assertEquals(captor.getValue().getConvertedUrl(), linkConverterDto.getConvertedUrl());

        ShortLink shortLink = new ShortLink("http://merced.es/GUKA8w");
        LongLink longLink = new LongLink("https://group.mercedes-benz.com/karriere/berufserfahrene/direkteinstieg");

        uri = shortLinkToLongConverterService.getLink(shortLink);

        assertThat(longLink.getUrl()).isEqualTo(uri.getUrl());

    }


}
