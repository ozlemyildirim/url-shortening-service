package com.mbm.link.converter.service.visitor.toshortlink;

import com.mbm.link.converter.linkconverter.dto.ShortLink;
import com.mbm.link.converter.linkconverter.dto.Uri;
import com.mbm.link.converter.linkconverter.dto.UriVisitor;
import com.mbm.link.converter.linkconverter.dto.LongLink;
import com.mbm.link.converter.linkconverter.service.visitor.toshortlink.ProductUriToShortLinkVisitor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ProductUriToShortLinkVisitorTest {


    @TestConfiguration
    static class ProductUriToShortLinkVisitorTestContextConfiguration {

        @Bean
        public UriVisitor uriVisitor() {
            return new ProductUriToShortLinkVisitor();
        }
    }

    @Autowired
    private UriVisitor uriVisitor;

    @Test
    public void check_Method() throws Exception {
        LongLink longLink = new LongLink("https://group.mercedes-benz.com/karriere/berufserfahrene/direkteinstieg");
        boolean result = uriVisitor.check(longLink);
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void visit_Method() throws Exception {
        ShortLink shortLink = new ShortLink("http://merced.es/GUKA8w");
        LongLink longLink = new LongLink("https://group.mercedes-benz.com/karriere/berufserfahrene/direkteinstieg");


        Uri uri = uriVisitor.visit(longLink);
        assertThat(shortLink.getHost()).isEqualTo(uri.getHost());
        assertThat(shortLink.getScheme()).isEqualTo(uri.getScheme());
        assertThat(shortLink.getPathSegments().size()).isEqualTo(uri.getPathSegments().size());
    }


}
