package com.mbm.link.converter.linkconverter.dto;

import com.mbm.link.converter.linkconverter.model.ConversionType;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class LinkConverterDto implements Serializable {

    private String sourceUrl;
    private String convertedUrl;
    private ConversionType conversionType;

}
