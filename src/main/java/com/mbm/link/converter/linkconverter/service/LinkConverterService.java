package com.mbm.link.converter.linkconverter.service;

import com.mbm.link.converter.linkconverter.dto.Uri;
import com.mbm.link.converter.linkconverter.model.LinkConverter;
import com.mbm.link.converter.linkconverter.repository.LinkConverterRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
public interface LinkConverterService {
    Uri prepareLink(Uri sourceUri);
    Uri getLink(Uri sourceUri);
    Uri findBySourceUrl(Uri uri);

}
