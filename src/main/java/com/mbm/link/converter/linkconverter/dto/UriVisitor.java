package com.mbm.link.converter.linkconverter.dto;

public interface UriVisitor {

    boolean check(Uri uri);

    Uri visit(Uri uri);

}
