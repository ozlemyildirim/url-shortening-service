package com.mbm.link.converter.linkconverter.consumer;

import com.mbm.link.converter.linkconverter.dto.LinkConverterDto;
import com.mbm.link.converter.linkconverter.service.ConversionHistoryService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
@Service
public class LinkConverterConsumer {

    private final ConversionHistoryService conversionHistoryService;

    @Autowired
    public LinkConverterConsumer(ConversionHistoryService conversionHistoryService) {
        this.conversionHistoryService = conversionHistoryService;
    }


    @RabbitListener(queues = "link-converter-queue")
    public void handleLinkConverterMessage(LinkConverterDto linkConverterDto) {
        conversionHistoryService.saveConversion(linkConverterDto);
    }
}
