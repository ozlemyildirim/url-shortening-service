package com.mbm.link.converter.linkconverter.model;


import com.mbm.link.converter.common.model.Auditable;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */

@AllArgsConstructor
@Builder
@NoArgsConstructor
@Data
@Entity
@Table(name = "link_converter")
public class LinkConverter extends Auditable implements Serializable {

    @NotNull
    @Column(name = "source_url")
    private String sourceUrl;

    @NotNull
    @Column(name = "converted_url")
    private String convertedUrl;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "conversion_type", length = 150)
    private ConversionType conversionType;
}
