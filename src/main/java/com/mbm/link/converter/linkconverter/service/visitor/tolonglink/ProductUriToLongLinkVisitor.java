package com.mbm.link.converter.linkconverter.service.visitor.tolonglink;

import com.mbm.link.converter.linkconverter.dto.Uri;
import com.mbm.link.converter.linkconverter.dto.UriVisitor;
import com.mbm.link.converter.utils.UriBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mbm.link.converter.common.constants.Constants.*;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
@Component
public class ProductUriToLongLinkVisitor implements UriVisitor {

    private final Pattern pattern;

    {
        pattern = Pattern.compile("(.*)");
    }

    @Override
    public boolean check(Uri uri) {
        // merced.es/GUKA8w
        if (uri.getPathSegments().size() == 2) {
            Matcher matcher = pattern.matcher(uri.getPathSegments().get(1));
            return matcher.matches();
        }
        return true;
    }

    @Override
    public Uri visit(Uri uri) {
     return  null;
    }


}
