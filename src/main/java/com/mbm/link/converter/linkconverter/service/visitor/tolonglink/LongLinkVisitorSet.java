package com.mbm.link.converter.linkconverter.service.visitor.tolonglink;

import com.mbm.link.converter.linkconverter.dto.UriVisitor;
import com.mbm.link.converter.linkconverter.service.visitor.AbstractVisitorSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class LongLinkVisitorSet extends AbstractVisitorSet {

    private final ProductUriToLongLinkVisitor productUriToLongLinkVisitor;

    @Autowired
    public LongLinkVisitorSet(ProductUriToLongLinkVisitor productUriToLongLinkVisitor) {
        this.productUriToLongLinkVisitor = productUriToLongLinkVisitor;
    }

    @PostConstruct
    private void initVisitorSet() {
        visitors.add(productUriToLongLinkVisitor);
    }


    @Override
    public List<UriVisitor> getVisitors() {
        return this.visitors;
    }
}
