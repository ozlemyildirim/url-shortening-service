package com.mbm.link.converter.linkconverter.service;

import com.mbm.link.converter.linkconverter.model.LinkConverter;
import com.mbm.link.converter.linkconverter.service.visitor.tolonglink.LongLinkVisitorSet;
import com.mbm.link.converter.linkconverter.service.visitor.toshortlink.ShortLinkVisitorSet;
import com.mbm.link.converter.linkconverter.dto.UriVisitor;
import com.mbm.link.converter.linkconverter.model.ConversionType;
import com.mbm.link.converter.linkconverter.producer.LinkConverterProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
@Service
public class ShortLinkToLongLinkConverterService extends AbstractLinkConverterServiceImp {

    private final LongLinkVisitorSet longLinkVisitorSet;

    @Autowired
    public ShortLinkToLongLinkConverterService(LinkConverterProducer linkConverterProducer, LongLinkVisitorSet longLinkVisitorSet) {
        super(linkConverterProducer);
        this.longLinkVisitorSet = longLinkVisitorSet;
    }

    @Override
    protected List<UriVisitor> getVisitorList() {
        return longLinkVisitorSet.getVisitors();
    }

    @Override
    protected ConversionType getConversionType() {
        return ConversionType.SHORTLINK_TO_LONG;
    }


}
