package com.mbm.link.converter.linkconverter.service.visitor.toshortlink;

import com.mbm.link.converter.linkconverter.dto.UriVisitor;
import com.mbm.link.converter.linkconverter.service.visitor.AbstractVisitorSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class ShortLinkVisitorSet extends AbstractVisitorSet {

    private final ProductUriToShortLinkVisitor productUriToShortLinkVisitor;

    @Autowired
    public ShortLinkVisitorSet(ProductUriToShortLinkVisitor productUriToShortLinkVisitor) {
        this.productUriToShortLinkVisitor = productUriToShortLinkVisitor;
    }

    @PostConstruct
    private void initVisitorSet() {
        visitors.add(productUriToShortLinkVisitor);
    }


    @Override
    public List<UriVisitor> getVisitors() {
        return this.visitors;
    }
}
