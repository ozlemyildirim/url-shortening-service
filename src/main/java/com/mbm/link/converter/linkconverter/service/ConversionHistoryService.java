package com.mbm.link.converter.linkconverter.service;

import com.mbm.link.converter.linkconverter.dto.LinkConverterDto;
import com.mbm.link.converter.linkconverter.model.LinkConverter;
import org.springframework.stereotype.Service;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
public interface ConversionHistoryService {
    void saveConversion(LinkConverterDto linkConverterDto);
}
