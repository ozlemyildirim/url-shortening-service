package com.mbm.link.converter.linkconverter.api;

import com.mbm.link.converter.common.api.dto.AppResponse;
import com.mbm.link.converter.linkconverter.dto.Uri;
import com.mbm.link.converter.linkconverter.dto.LongLink;
import com.mbm.link.converter.linkconverter.model.LinkConverter;
import com.mbm.link.converter.linkconverter.service.LongLinkToShortLinkConverterService;
import com.mbm.link.converter.linkconverter.service.ShortLinkToLongLinkConverterService;
import com.mbm.link.converter.common.api.BaseController;
import com.mbm.link.converter.linkconverter.dto.ShortLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = BaseController.BASE_PATH + "/convert")
public class LinkConverterRestController {

    private final LongLinkToShortLinkConverterService longLinkToShortLinkConverterService;
    private final ShortLinkToLongLinkConverterService shortLinkToLongLinkConverterService;

    @Autowired
    public LinkConverterRestController(LongLinkToShortLinkConverterService longLinkToShortLinkConverterService, ShortLinkToLongLinkConverterService shortLinkToLongLinkConverterService) {
        this.longLinkToShortLinkConverterService = longLinkToShortLinkConverterService;
        this.shortLinkToLongLinkConverterService = shortLinkToLongLinkConverterService;
    }

    @PostMapping("/longlink-to-shortlink")
    public AppResponse<String> longlinkToShortlink(@Valid @RequestBody LongLink longLink) {
        Uri uri = longLinkToShortLinkConverterService.prepareLink(longLink);
        return AppResponse.ok().addData(uri.getUrl());
    }

    @GetMapping ("/shortlink-to-longlink")
    public AppResponse<String> shortlinkToLonglink(@Valid @RequestBody ShortLink shortLink) {
        Uri uri = shortLinkToLongLinkConverterService.getLink(shortLink);
        return AppResponse.ok().addData(uri.getUrl());

    }
}
