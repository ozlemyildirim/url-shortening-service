package com.mbm.link.converter.linkconverter.service;

import com.mbm.link.converter.common.exception.ServiceException;
import com.mbm.link.converter.linkconverter.dto.*;
import com.mbm.link.converter.linkconverter.model.ConversionType;
import com.mbm.link.converter.linkconverter.model.LinkConverter;
import com.mbm.link.converter.linkconverter.producer.LinkConverterProducer;
import com.mbm.link.converter.linkconverter.repository.LinkConverterRepository;
import com.mbm.link.converter.utils.UriBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
public abstract class AbstractLinkConverterServiceImp implements LinkConverterService {

    @Autowired
    LinkConverterRepository linkConverterRepository;

    private final LinkConverterProducer linkConverterProducer;

    AbstractLinkConverterServiceImp(LinkConverterProducer linkConverterProducer) {
        this.linkConverterProducer = linkConverterProducer;
    }

    public Uri prepareLink(Uri sourceUri) {
        // firstly we are converting the Uri
        Uri convertedUri = convertLink(sourceUri);
        // create event to save conversation
        createSaveConversionEvent(sourceUri, convertedUri);
        return convertedUri;
    }


    public Uri getLink(Uri convertedUri) {
        List<UriVisitor> visitorList = getVisitorList();
        Optional<UriVisitor> selectedVisitor = visitorList.stream().filter(uriVisitor -> uriVisitor.check(convertedUri)).findFirst();
        if (!selectedVisitor.isPresent()) {
            throw new ServiceException("error.invalid.page.to.convert");
        }
        return findBySourceUrl(convertedUri);
    }


    protected abstract List<UriVisitor> getVisitorList();

    protected abstract ConversionType getConversionType();


    private Uri convertLink(Uri uri) {
        List<UriVisitor> visitorList = getVisitorList();
        Optional<UriVisitor> selectedVisitor = visitorList.stream().filter(uriVisitor -> uriVisitor.check(uri)).findFirst();
        // default one is HomePage buf if there is a problem we response back a message.
        if (!selectedVisitor.isPresent()) {
            throw new ServiceException("error.invalid.page.to.convert");
        }
        return selectedVisitor.get().visit(uri);
    }

    private void createSaveConversionEvent(Uri sourceUri, Uri convertedUri) {
        ConversionType conversionType = getConversionType();
        LinkConverterDto linkConverterDto = LinkConverterDto
                .builder()
                .conversionType(conversionType)
                .sourceUrl(sourceUri.getUrl())
                .convertedUrl(convertedUri.getUrl())
                .build();
        linkConverterProducer.sendToQueue(linkConverterDto);

    }




    @Transactional(propagation = Propagation.REQUIRED)
    public Uri findBySourceUrl(Uri convertedUri) {
        LinkConverter linkConverter = linkConverterRepository.findByConvertedUrl(convertedUri.getUrl());
        LongLink longLink = new LongLink(linkConverter.getSourceUrl());
        return longLink;
    }

}
