package com.mbm.link.converter.linkconverter.dto;

import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@NoArgsConstructor
public class LongLink extends Uri {

    public LongLink(String url) {
        super(url);
    }

    @Override
    @Pattern(regexp = "https?", message = "{scheme.notcorrect}")
    @NotEmpty(message = "{scheme.notempty}")
    public String getScheme() {
        return super.getScheme();
    }

    @Override
    @NotEmpty(message = "{host.notempty}")
    public String getHost() {
        return super.getHost();
    }

}
