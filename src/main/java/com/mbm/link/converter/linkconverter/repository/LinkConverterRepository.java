package com.mbm.link.converter.linkconverter.repository;

import com.mbm.link.converter.linkconverter.model.LinkConverter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
@Repository
public interface LinkConverterRepository extends JpaRepository<LinkConverter, Long> {
    LinkConverter findByConvertedUrl(String convertedUrl);
}
