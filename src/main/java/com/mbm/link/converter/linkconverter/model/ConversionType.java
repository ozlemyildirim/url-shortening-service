package com.mbm.link.converter.linkconverter.model;

public enum ConversionType {
	LONG_TO_SHORTLINK,
    SHORTLINK_TO_LONG
}
