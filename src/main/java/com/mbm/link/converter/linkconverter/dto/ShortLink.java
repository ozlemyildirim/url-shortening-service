package com.mbm.link.converter.linkconverter.dto;

import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@NoArgsConstructor
public class ShortLink extends Uri {

    public ShortLink(String url) {
        super(url);
    }

    @Override
    @NotEmpty(message = "{scheme.notempty}")
    public String getScheme() {
        return super.getScheme();
    }

}
