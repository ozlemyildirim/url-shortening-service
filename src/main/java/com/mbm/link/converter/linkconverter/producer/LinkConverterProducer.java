package com.mbm.link.converter.linkconverter.producer;

import com.mbm.link.converter.linkconverter.dto.LinkConverterDto;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
@Service
public class LinkConverterProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${link.converter.rabbit.routing.name}")
    private String routingName;

    @Value("${exchange.direct}")
    private String exchangeName;

    public void sendToQueue(LinkConverterDto linkConverterDto) {
        rabbitTemplate.convertAndSend(exchangeName, routingName, linkConverterDto);
    }

}
