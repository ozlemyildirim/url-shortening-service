package com.mbm.link.converter.linkconverter.mapper;

import com.mbm.link.converter.linkconverter.model.LinkConverter;
import com.mbm.link.converter.linkconverter.dto.LinkConverterDto;
import org.mapstruct.Mapper;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
@Mapper(componentModel = "spring")
public abstract class ConversionMapper {

    public abstract LinkConverter createLinkConverter(LinkConverterDto linkConverterDto);

}
