package com.mbm.link.converter.linkconverter.service;

import com.mbm.link.converter.linkconverter.model.LinkConverter;
import com.mbm.link.converter.linkconverter.service.visitor.tolonglink.LongLinkVisitorSet;
import com.mbm.link.converter.linkconverter.dto.UriVisitor;
import com.mbm.link.converter.linkconverter.model.ConversionType;
import com.mbm.link.converter.linkconverter.producer.LinkConverterProducer;
import com.mbm.link.converter.linkconverter.service.visitor.toshortlink.ShortLinkVisitorSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
@Service
public class LongLinkToShortLinkConverterService extends AbstractLinkConverterServiceImp {

    private final ShortLinkVisitorSet shortLinkVisitorSet;

    @Autowired
    public LongLinkToShortLinkConverterService(LinkConverterProducer linkConverterProducer, ShortLinkVisitorSet shortLinkVisitorSet) {
        super(linkConverterProducer);
        this.shortLinkVisitorSet = shortLinkVisitorSet;
    }

    @Override
    protected List<UriVisitor> getVisitorList() {
        return shortLinkVisitorSet.getVisitors();
    }

    @Override
    protected ConversionType getConversionType() {
        return ConversionType.LONG_TO_SHORTLINK;
    }


}
