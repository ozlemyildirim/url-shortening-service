package com.mbm.link.converter.linkconverter.service;

import com.mbm.link.converter.linkconverter.dto.Uri;
import com.mbm.link.converter.linkconverter.repository.LinkConverterRepository;
import com.mbm.link.converter.linkconverter.dto.LinkConverterDto;
import com.mbm.link.converter.linkconverter.mapper.ConversionMapper;
import com.mbm.link.converter.linkconverter.model.LinkConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
@Service
public class ConversionHistoryServiceImp implements ConversionHistoryService {

    private final LinkConverterRepository linkConverterRepository;
    private final ConversionMapper conversionMapper;

    @Autowired
    public ConversionHistoryServiceImp(LinkConverterRepository linkConverterRepository, ConversionMapper conversionMapper) {
        this.linkConverterRepository = linkConverterRepository;
        this.conversionMapper = conversionMapper;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void saveConversion(LinkConverterDto linkConverterDto) {
        LinkConverter linkConverter = conversionMapper.createLinkConverter(linkConverterDto);
        linkConverterRepository.save(linkConverter);
    }

}
