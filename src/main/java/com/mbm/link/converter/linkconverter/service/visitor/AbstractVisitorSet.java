package com.mbm.link.converter.linkconverter.service.visitor;

import com.mbm.link.converter.linkconverter.dto.UriVisitor;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractVisitorSet {

    protected List<UriVisitor> visitors;

    public abstract List<UriVisitor> getVisitors();

    public AbstractVisitorSet() {
        this.visitors = new LinkedList<>();
    }

}
