package com.mbm.link.converter.linkconverter.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
public abstract class Uri {

    private String url;
    protected String host;
    protected String scheme;
    protected List<String> pathSegments;
    protected Map<String, String> params;

    Uri(String url) {
        this.setUrl(url);
    }

    public void setUrl(String url) {
        this.url = url;
        this.init();
    }

    private void init() {
        UriComponents uriComponents = UriComponentsBuilder.fromUriString(url).build();
        this.scheme = uriComponents.getScheme();
        this.host = uriComponents.getHost();
        this.pathSegments = uriComponents.getPathSegments();
        this.params = uriComponents.getQueryParams().entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().stream().findFirst().orElse(null)));
    }

}
