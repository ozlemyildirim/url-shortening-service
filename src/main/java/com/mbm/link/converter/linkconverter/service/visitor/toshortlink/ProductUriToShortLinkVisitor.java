package com.mbm.link.converter.linkconverter.service.visitor.toshortlink;

import com.mbm.link.converter.common.constants.Constants;
import com.mbm.link.converter.linkconverter.dto.ShortLink;
import com.mbm.link.converter.linkconverter.dto.Uri;
import com.mbm.link.converter.linkconverter.dto.UriVisitor;
import com.mbm.link.converter.utils.UriBuilder;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mbm.link.converter.common.constants.Constants.*;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
@Component
public class ProductUriToShortLinkVisitor implements UriVisitor {

    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private Random random = new Random();

    private final Pattern pattern;

    {
        pattern = Pattern.compile("(.*)");
    }


    @Override
    public boolean check(Uri uri) {
         // group.mercedes-benz.com/karriere/berufserfahrene/direkteinstieg
       return uri.getUrl().contains(Constants.MERCEDES_BENZ_HOST_NAME);
    }

    @Override
    public ShortLink visit(Uri uri) {
        String shortLink;
        UriBuilder uriBuilder = UriBuilder.create();

        char[] result = new char[6];
                for (int i = 0; i < 6; i++) {
                    int randomIndex = random.nextInt(ALPHABET.length() - 1);
                    result[i] = ALPHABET.charAt(randomIndex);
                }
                  shortLink = new String(result);
                // make sure the short link isn't already used
        uriBuilder.addPath(SEGMENT).addPath(shortLink);

        return uriBuilder.buildShortLink();
    }

}
