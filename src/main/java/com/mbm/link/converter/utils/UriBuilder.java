package com.mbm.link.converter.utils;

import com.mbm.link.converter.common.constants.Constants;
import com.mbm.link.converter.linkconverter.dto.ShortLink;
import com.mbm.link.converter.linkconverter.dto.LongLink;
import org.springframework.web.util.UriComponentsBuilder;

public class UriBuilder {
    private final UriComponentsBuilder builder = UriComponentsBuilder.newInstance();

    public static UriBuilder create() {
        return new UriBuilder();
    }

    public UriBuilder addPath(String path) {
        builder.path(path);
        return this;
    }

    public UriBuilder addParam(String key, String value) {
        builder.queryParam(key, value);
        return this;
    }

    public ShortLink buildShortLink() {
        String url = this.build(Constants.SCHEMA_SECURE_SHORT, Constants.SCHEMA_SHORT);
        return new ShortLink(url);
    }

    public LongLink buildLongLink() {
        String url = this.build(Constants.SCHEMA_SECURE_LONG, Constants.MERCEDES_BENZ_HOST_NAME);
        return new LongLink(url);
    }

    private String build(String schema, String host) {
        return builder
                .scheme(schema)
                .host(host)
                .build()
                .toUriString();
    }


}
