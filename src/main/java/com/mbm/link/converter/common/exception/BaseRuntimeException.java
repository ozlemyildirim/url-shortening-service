package com.mbm.link.converter.common.exception;


import static org.hibernate.internal.util.collections.ArrayHelper.EMPTY_OBJECT_ARRAY;

public abstract class BaseRuntimeException extends RuntimeException {
    private String msgKey;
    private Object[] msgArgs;

    BaseRuntimeException() {
        this.msgArgs = EMPTY_OBJECT_ARRAY;
    }

    BaseRuntimeException(String message, Throwable cause) {
        super(message, cause);
        this.msgArgs = EMPTY_OBJECT_ARRAY;
    }

    BaseRuntimeException(String message) {
        super(message);
        this.msgArgs = EMPTY_OBJECT_ARRAY;
        this.setMsgKey(message);
    }

    BaseRuntimeException(Throwable cause) {
        super(cause);
        this.msgArgs = EMPTY_OBJECT_ARRAY;
    }

    BaseRuntimeException(String msgKey, Object... msgArgs) {
        this.msgArgs = EMPTY_OBJECT_ARRAY;
        this.setMsgKey(msgKey);
        this.setMsgArgs(msgArgs);
    }

    BaseRuntimeException(String message, Throwable cause, String msgKey, Object... msgArgs) {
        super(message, cause);
        this.msgArgs = EMPTY_OBJECT_ARRAY;
        this.setMsgKey(msgKey);
        this.setMsgArgs(msgArgs);
    }

    BaseRuntimeException(String message, String msgKey, Object... msgArgs) {
        super(message);
        this.msgArgs = EMPTY_OBJECT_ARRAY;
        this.setMsgKey(msgKey);
        this.setMsgArgs(msgArgs);
    }

    BaseRuntimeException(Throwable cause, String msgKey, Object... msgArgs) {
        super(cause);
        this.msgArgs = EMPTY_OBJECT_ARRAY;
        this.setMsgKey(msgKey);
        this.setMsgArgs(msgArgs);
    }

    public String getMsgKey() {
        return this.msgKey;
    }

    public void setMsgKey(String msgKey) {
        this.msgKey = msgKey;
    }

    public Object[] getMsgArgs() {
        return this.msgArgs == null ? EMPTY_OBJECT_ARRAY : this.msgArgs;
    }

    public void setMsgArgs(Object[] msgArgs) {
        this.msgArgs = msgArgs;
    }
}
