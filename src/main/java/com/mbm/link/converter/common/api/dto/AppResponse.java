package com.mbm.link.converter.common.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.HashSet;
import java.util.Set;

@Getter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AppResponse<T> {
    private final Integer status;
    private T data;
    private final Set<String> errorMessages = new HashSet<>();

    private AppResponse(Integer status) {
        this.status = status;
    }

    public static AppResponse ok() {
        return new AppResponse(HttpStatus.OK.value());
    }

    public static AppResponse error(HttpStatus httpStatus) {
        return new AppResponse(httpStatus.value());
    }

    public AppResponse addData(T data) {
        this.data = data;
        return this;
    }

    public AppResponse addMessage(String message) {
        this.errorMessages.add(message);
        return this;
    }

    public AppResponse addMessages(Set<String> messages) {
        this.errorMessages.addAll(messages);
        return this;
    }
}
