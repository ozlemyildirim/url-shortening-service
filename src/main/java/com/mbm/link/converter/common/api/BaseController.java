package com.mbm.link.converter.common.api;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class BaseController {
    public static final String BASE_PATH = "/api";
}
