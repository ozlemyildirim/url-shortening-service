package com.mbm.link.converter.common.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Locale;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants {

    // default languages
    public static final Locale ENGLISH = new Locale("en", "");

    public static final String UTF_8 = "UTF-8";

    // host names
    public static final String MERCEDES_BENZ_HOST_NAME = "group.mercedes-benz.com";

    // schemes
    public static final String SCHEMA_SECURE_LONG = "https";

    public static final String SCHEMA_SECURE_SHORT = "http";

    public static final String SCHEMA_SHORT = "merced.es";


    // segments paths
    public static final String SEGMENT = "/";


}
