package com.mbm.link.converter.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * User: ozlyild
 * Date: 12.06.2022
 */
@Configuration
public class RabbitMqConfiguration {

    @Value("${exchange.direct}")
    private String exchangeName;

    @Value("${link.converter.rabbit.queue.name}")
    private String linkConverterQueueName;

    @Value("${link.converter.rabbit.routing.name}")
    private String linkConverterRoutingName;

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(exchangeName);
    }

    @Bean
    public Queue queue() {
        return new Queue(linkConverterQueueName);
    }

    @Bean
    Binding bindingQueue() {
        return BindingBuilder.bind(queue()).to(directExchange()).with(linkConverterRoutingName);
    }

}
