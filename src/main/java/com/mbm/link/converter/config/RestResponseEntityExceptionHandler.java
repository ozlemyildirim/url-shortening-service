package com.mbm.link.converter.config;

import com.mbm.link.converter.common.api.dto.AppResponse;
import com.mbm.link.converter.common.exception.ServiceException;
import com.mbm.link.converter.utils.LocaleAwareMessageProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Author: ozlyild
 * Date: 12.06.2022
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private LocaleAwareMessageProvider messageProvider;

    @Autowired
    public RestResponseEntityExceptionHandler(LocaleAwareMessageProvider messageProvider) {
        this.messageProvider = messageProvider;
    }

    @ExceptionHandler(value = {ServiceException.class})
    protected ResponseEntity<Object> handleBusinessExceptions(ServiceException ex, WebRequest request) {
        String errorMessage = this.messageProvider.getMessage(ex.getMsgKey(), ex.getMsgArgs());
        logger.debug(errorMessage, ex);
        return handleExceptionInternal(ex, AppResponse.error(HttpStatus.BAD_REQUEST).addMessage(errorMessage), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity<Object> handleRuntimeExceptions(RuntimeException ex, WebRequest request) {
        return logMessages(ex, request, "error.java.lang.Throwable");
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Set<String> errors = ex.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toSet());
        return handleExceptionInternal(ex, AppResponse.error(HttpStatus.BAD_REQUEST).addMessages(errors), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    private ResponseEntity<Object> logMessages(Exception ex, WebRequest request, String messageKey) {
        String errorKey = "FK-" + UUID.randomUUID().toString();
        String errorMessage = messageProvider.getMessage(messageKey) + " (" + errorKey + ")";
        logger.error(errorKey, ex);
        return handleExceptionInternal(ex, AppResponse.error(HttpStatus.INTERNAL_SERVER_ERROR).addMessage(errorMessage), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
